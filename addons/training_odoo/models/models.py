# -*- coding: utf-8 -*-

from odoo import models, fields, api


class training_course(models.Model):
    _name = 'training.course'
    _description = 'deskripsi course'

    name = fields.Char(string='Judul', required=True)
    # value = fields.Integer()
    # value2 = fields.Float(compute="Judul", store=True)
    description = fields.Text(string='Keterangan')
    user_id = fields.Many2one('res.users', string="Penanggung Jawab")
    product_ids = fields.Many2many('product.product', 'course_product_rel', 'course_id',
'product_id', 'Cendera Mata')
    # # @api.depends('value')
    # # def _value_pc(self):
    # #     for record in self:
    # #         record.value2 = float(record.value) / 100
class TrainingSession(models.Model):
    _name = 'training.session'
    _description = 'Training Sesi'

    course_id = fields.Many2one('training.course', string='Judul Kursus', required=True, ondelete='cascade')
    name = fields.Char(string='Nama', required=True)
    start_date = fields.Date(string='Tanggal')
    duration = fields.Float(string='Durasi', help='Jumlah Hari Training') 
    seats = fields.Integer(string='Kursi', help='Jumlah Kuota Kursi')
    partner_id = fields.Many2one('res.partner', string='Instruktur')